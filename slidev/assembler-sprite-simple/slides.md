---
theme: seriph
background:  http://lamyoung.gitee.io/web/slidev/assembler-sprite-simple/img/bg.jpg
class: text-center
highlighter: shiki
download: true
info: |
  ## assembler
---

# Cocos Star Meetings
## 开发者们的聚会 
### 每个参与者都是现场的主角!


<a href="https://github.com/baiyuwubing" target="_blank" alt="GitHub"
  class="abs-br m-6 text-xl icon-btn opacity-50 !border-none !hover:text-white">
  <carbon-logo-github />
</a> 


---

# 如何实现

思考


<img
  class="relative -bottom-0 -left-10 w-80"
  src="/img/folding.gif"
/>

<img
  class="relative -top-80 -right-120  w-150"
  src="/img/colorful.gif"
/>

<img
  class="relative -top-160 -right-70 w-80"
  src="/img/verticalLabel.gif"
/>




---

# Assembler
by 白玉无冰

- 概念
- 实战
- 交流
- 扫码

![](/img/whowhere.jpg)


---

# 我是谁
- 组？ 组织什么？
- 装？ 装到哪去？
- 器？？

![](/img/who.jpg)  


---

# 我从哪里来
何处是我家？ **Renderabale2D**

<img
  class="relative -top-0 -left-0 w-100"
  src="/img/renderabale2d.jpg"
/>

<img
  class="relative -top-100 -right-120 w-100"
  src="/img/renderabale2d2.jpg"
/>



---

# 我要到哪去
**sprite-simple** 组装的调用时机？

![](/img/sprite-simple.jpg)  
 
---

# 我要到哪去
**MeshBuffer** 里是怎么装的？

![](/img/meshbuffer.jpg)  
 

---

# 动手改一改

实践一下！

<img src="/img/doit.gif"  width="480"/>

---

# 动手改一改1
- 组装器模版:源码

![](/img/sprite-simple.png)  

---

# 动手改一改2

- 继承一个 **Renderabale2D** ，并指向组装器，添加属性


![](/img/exSprite.jpg)  

---

# 动手改一改3
- 改造组装器:组

![](/img/updateVert.jpg)  

---

# 动手改一改3
- 改造组装器:装

<img src="/img/fillbuffers.jpg"  width="720"/>

---

# 动手改一改
- 准备组装器模版: 源码 
- 继承一个 **Renderabale2D** ，并指向组装器，添加属性
- 改造组装器

![](/img/doit.gif)  

---


# 交流互动
- 多边形顶点索引怎么计算 ？
- assembler 与 shader 的区别？
- 什么ccc版本可以用？
- ...

## 参考链接
[https://forum.cocos.org/t/topic/95087](https://forum.cocos.org/t/topic/95087)  
[https://docs.cocos.com/creator/manual/zh/advanced-topics/custom-render.html?](https://docs.cocos.com/creator/manual/zh/advanced-topics/custom-render.html?)  



---


## 你的能力绝不是一个定值，在学习的世界里，一切皆有可能

感谢观看

<img
  class="absolute -bottom-0 -left-0 w-70"
  src="/img/me.png"
/>

<img
  class="absolute -bottom-0 -right-0 w-80"
  src="/img/qrcode.jpg"
/>
